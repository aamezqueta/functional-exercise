const fibonacci = number => {
    if ( number > 2 ) {
      return fibonacci(number - 1) + fibonacci(number - 2);
    }
    else if ( number > 0 ){
      return 1;
    }
    else{
      return 0;
    }
};

module.exports = fibonacci;
