const faverage = numbers => {

    if ( numbers === undefined ){
      return 0;
    }

    if (numbers.length === 0 ){
      return 0;
    }
    
    return numbers
      .reduce( (accumulator, currentValue) => accumulator + currentValue, 0 )/ numbers.length;
};

module.exports = faverage;
