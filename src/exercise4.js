const fillDict = (dict,key)=>{
  (dict[key] === undefined)? ( dict[key] = 1 ):( dict[key]++);
}

const createDict = (sequence) =>{

    let dict = {};
    sequence.split(" ").filter(string => string !== '')
    .map(string => string.toLowerCase())
    .forEach(key => fillDict(dict,key));

    return dict;
}

const wordCount = sequence => {
  
  let result = {};

  try{
    result = createDict(sequence);
  }
  catch ( TypeError ){
    return null;
  }
  return (Object.keys(result).length !== 0)? result : null;
};

module.exports = {
  wordCount,
};
